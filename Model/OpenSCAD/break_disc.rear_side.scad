/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use <../../Archetype/OpenSCAD/break_disc.scad>

ratio = 1/87;
scale_vector = [ratio, ratio, ratio];

/**
 * 2D cut from the scaled archetype: rear side of the disc.
 */
module break_disc_rear_side() {
  // We need to move this in z axis due to the chamfer on the back side
  // The thickness of this sheet is 0.1, so this is only a part of archetype's
  // complete thickness.
  projection(cut = true) scale(scale_vector) translate([0, 0, -15]) break_disc();
}

break_disc_rear_side($fn = 50);
