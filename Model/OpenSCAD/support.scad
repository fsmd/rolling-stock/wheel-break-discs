/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Support to center the disk onto the axle.
 */
module support() {
  bar_width = 1.7;
  bar_length = 5;
  bar_count = 3;
  union() {
    for (i = [0:bar_count - 1]) {
      rotate(360/bar_count * i) _bar();
    }
    circle(d = 2.5);
  }

  module _bar() {
    fit_hole_diameter = 0.3;
    translate([0, bar_length / 2])
    difference() {
      square([bar_width, bar_length], true);
      circle(d = fit_hole_diameter);
    }
  }
}

support($fn = 50);
