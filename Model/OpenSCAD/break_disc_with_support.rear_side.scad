/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use <break_disc.rear_side.scad>
use <support.scad>

/**
 * Adds supports/holder to the break disk.
 *
 * Only connected on the rear side!
 */
module break_disc_with_support_rear_side() {
  break_disc_rear_side();
  _support_with_center_hole();

  module _support_with_center_hole() {
    axle_hole_diameter = 1.5;
    difference() {
      support();
      circle(d = axle_hole_diameter);
    }
  }
}

break_disc_with_support_rear_side($fn = 50);
