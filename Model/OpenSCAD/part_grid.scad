/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Module to arrange one part multiple times.
 * To connect and surround them with a frame.
 */
module part_grid(columns = 2, rows = 4, distance_rows = 14, distance_columns = 14, connector_length = 2, connector_width = 1, connectors = true, frame = true) {
  union() {
    // Columns
    for (i = [0:columns - 1]) {
      translate([0, distance_columns * i]) {
        // Rows
        for (i = [0:rows - 1]) {
          translate([distance_rows * i, 0]) children(0);
        }
      }
    }

    if (connectors) {
      _row_connectors();
      _column_connectors();
    }

    if (frame) {
      _frame();
    }
  }

  module _row_connectors() {
    for (i = [0:columns - 1]) {
      translate([0, distance_columns * i]) {
        // Rows
        for (i = [-1:rows - 1]) {
          translate([distance_rows * i, 0]) translate([distance_rows / 2, 0]) square([connector_length, connector_width], true);
        }
      }
    }
  }

  module _column_connectors() {
    for (i = [-1:columns - 1]) {
      translate([0, distance_columns * i]) {
        // Rows
        for (i = [0:rows - 1]) {
          translate([distance_rows * i, 0]) translate([0, distance_columns / 2]) square([connector_width, connector_length], true);
        }
      }
    }
  }

  module _frame(width = 3) {
    inner_length = rows * distance_rows;
    inner_width = columns * distance_columns;
    outer_length = inner_length + 2 * width;
    outer_width = inner_width + 2 * width;

    difference() {
      translate([- distance_rows / 2 - width, - distance_columns / 2 - width]) square([outer_length, outer_width]);
      translate([- distance_rows / 2, - distance_columns / 2]) square([inner_length, inner_width]);
    }
  }
}
