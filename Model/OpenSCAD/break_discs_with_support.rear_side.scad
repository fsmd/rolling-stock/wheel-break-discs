/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use <break_disc_with_support.rear_side.scad>
use <part_grid.scad>

/**
 * Disk with support 8 times.
 *
 * The connectors are only on the rear side!
 */
module break_discs_with_support_rear_side() {
  part_grid(connector_width = 0.6) break_disc_with_support_rear_side();
}

break_discs_with_support_rear_side($fn = 20);
