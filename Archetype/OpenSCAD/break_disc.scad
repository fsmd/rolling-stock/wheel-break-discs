/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
  * A break disk of modern electric locomotives.
  * Radbremsscheibe einer modernen E-Lok (TRAXX u. a.)
  *
  * @Todo: make this general with parameters for radius/diameter, holes...
  *
  * Sources:
  * [1] Measurement by Roland Zedelmayer (03.2020)
  * [2] Measurement from drawing 1 Fle 146.0.02.001.009
  */
module break_disc() {
  outer_radius = 1085 / 2;
  inner_radius = 805 / 2;
  thickness = 24;
  hole_diameter = 27; // Taken from [1]
  hole_head_diameter = 35; // Taken from [2]
  screw_head_af = 22; // Width across flats (Schlüsselweite), taken from [2]
  screw_head_height = 8; // Taken from [2]

  difference() {
    _disc();
    _holes();
  }

  module _disc() {
    rotate_extrude() translate([inner_radius, 0]) _shape();
    module _shape() {
      // Values taken from [2]
      points = [
        [0, 5],
        [5, 0],
        [135, 0],
        [140, 5],
        [140, thickness],
        [0, thickness],
      ];
      polygon(points);
    }
  }

  module _holes() {
    circular_path_radius = (outer_radius - inner_radius) / 2 + inner_radius;
    count = 18;
    for (i = [0:count]) {
      rotate(360 / count * i) translate([circular_path_radius, 0, 0]) _hole();
    }

    module _hole() {
      union() {
        cylinder(d = hole_diameter, h = thickness * 1.2);
        translate([0, 0, thickness - hole_head_diameter / 2]) cylinder(d1 = 0, d2 = hole_head_diameter * 1.1, h = hole_head_diameter * 1.1 / 2);
      }
    }
  }
}

color("silver") break_disc($fn = 100);
