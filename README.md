# Wheel Break Discs for Locomotives and xMUs
# Radbremsscheiben für Triebfahrzeuge

Archetype drawings and model implementations of modern wheel break disks.
Bombardier TRAXX in H0 at first.

Vorbildzeichnungen und -konstruktionen und Modellumsetzungen von modernen Radscheibenbremsen.
Zunächst für die TRAXX in H0.

## Dimensions

### Traxx F140
(Own inquiries)

* Outer diameter: 1040 mm (1.5 mm champfer)
* Inner diameter: 750 mm (5 mm champfer = 740 mm)
* Thickness over all: 52 mm
* Thickness disc itself: 24 mm, 5 mm wear-layer

### Br 152
(From "EuroSpriter")

* Outer diameter: 1050 mm
* Inner diameter: 750 mm

## Generate SVG, process to edging

### OpenSCAD ⇒ SVG

To generate the SVG files it's recommended to set the `$fn` variable for smother shapes. This leads to long computing time so it is recommended to use the OpenSCAD from the command line.

**Example**

```
$ openscad -o Model/SVG/break_disc_with_support.rear_side.svg Model/OpenSCAD/break_disc_with_support.rear_side.scad -D '$fn=150'
```

### SVG ⇒ Illustrator

Open the SVG file with Illustrator and generate an AI-file according to the standards (tba!).

### Illustrator ⇒ PDF

Arrange all parts of one metal sheet, render all fonts and save it as PDF file.
